package tn.esprit;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Role;
import tn.esprit.entities.User;
import tn.esprit.service.IUserRemoteService;



public class MainQuestion1 {

	private static IUserRemoteService ProxyUser;

	public static void addUser(User u)
	{
		int uu=ProxyUser.addUser(u);
		System.out.println("USer "+uu);
	}
	
	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		String jndiName = "examen2019-ear/examen2019-ejb/UserService!tn.esprit.service.IUserRemoteService";
		Context context = new InitialContext();
		ProxyUser=((IUserRemoteService) context.lookup(jndiName));
		
		
		
		User u =new User();
		u.setNom("Mohamed");
		u.setPrenom("Bouhlel");
		u.setEmail("ab@esprit.tn");
		u.setPassword("dev");
		u.setRole(Role.DEVELOPER);
		
		
		User uu =new User();
		uu.setNom("Kais");
		uu.setPrenom("Allani");
		uu.setEmail("ka@esprit.tn");
		uu.setPassword("cli");
		uu.setRole(Role.CLIENT);
		
		addUser(u);
		addUser(uu);
		
		
	}

}
