package tn.esprit;

import java.util.List;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Project;
import tn.esprit.entities.User;
import tn.esprit.service.Impl.IProjectServiceRemote;

public class Question4 {

	private static IProjectServiceRemote ProxyProject;

	public static List<Project> getALLProject()
	{
		List<Project> projects=ProxyProject.findAll();
		System.out.println(projects);
		return projects;
		
	}
	
	
	public static List<Project> getPRojectByUser(int id)
	{
		List<Project> projects=ProxyProject.findAll();
		for (Project p : projects) {
			for (User u : p.getUsers()) {
				if (u.getId()==id) {
					System.out.println(p);
				}
				}
		}
		return null;
		
	}
	
	
	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		String jndiName = "examen2019-ear/examen2019-ejb/ProjectService!tn.esprit.service.Impl.IProjectServiceRemote";
		Context context = new InitialContext();
		ProxyProject=((IProjectServiceRemote) context.lookup(jndiName));
		
		getALLProject();
		getPRojectByUser(2);
	}

}
