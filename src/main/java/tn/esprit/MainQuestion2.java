package tn.esprit;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Project;
import tn.esprit.service.Impl.IProjectServiceRemote;



public class MainQuestion2 {

	private static IProjectServiceRemote ProxyProject;
	
	public static void addProject(Project p)
	{
		Project uu=ProxyProject.add(p);
		System.out.println("Projet "+uu.getId());
	}

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		String jndiName = "examen2019-ear/examen2019-ejb/ProjectService!tn.esprit.service.Impl.IProjectServiceRemote";
		Context context = new InitialContext();
		ProxyProject=((IProjectServiceRemote) context.lookup(jndiName));
		
		Project pp =new Project();
		pp.setDescription("Gestion de Mandats");
		pp.setTitle("MAP");
		
		Project p =new Project();
		p.setDescription("Gestion de OEuvres");
		p.setTitle("OTDAV");
		
		addProject(p);
		addProject(pp);
	}
	
	


}
