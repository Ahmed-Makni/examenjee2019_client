package tn.esprit;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Project;
import tn.esprit.entities.User;
import tn.esprit.service.IUserRemoteService;
import tn.esprit.service.Impl.IProjectServiceRemote;

public class MainQuestion3 {

	private static IProjectServiceRemote ProxyProject;
	private static IUserRemoteService ProxyUser;

	public static void assignProjectToUser(int projectId,int UserId) {
		User u=ProxyUser.findUserById(UserId);
		Project p=ProxyProject.find(projectId);
		u.getProjects().add(p);
		ProxyUser.updateUser(u);
		System.out.println("ok");
	}
	
	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		String jndiName = "examen2019-ear/examen2019-ejb/ProjectService!tn.esprit.service.Impl.IProjectServiceRemote";
		Context context = new InitialContext();
		ProxyProject=((IProjectServiceRemote) context.lookup(jndiName));
		
		String jndiName2 = "examen2019-ear/examen2019-ejb/UserService!tn.esprit.service.IUserRemoteService";
		Context context2 = new InitialContext();
		ProxyUser=((IUserRemoteService) context2.lookup(jndiName2));
		
		assignProjectToUser(1,2);
	}

}
